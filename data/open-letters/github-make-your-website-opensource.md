@gitbug,
Please make your website open sourced for private use. Here's why I ask.

Your website makes digital publishing of anything amazing. It brings repositories to life visually and interactively by sheer use of markdown rendering + the amazing commeting interface. It beats everything there is for text-based communication. It would be great to have one running on a local network, just for the people of my household for instance.

You could revolutionize how the majority of people communicate free and openly.